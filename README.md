# docker-workspaces-django

## Description

Django backend to create isolated Docker workspaces based on Django users. This project is for a trainning on Decidim App, but can work with other needs and other Docker containers.

When user is looged in Django and click on "Create" button, it happens the following:

- Creates a directory for the user
- Clone https://gitlab.com/abidueiro/decidim-hacks
- Create and Nginx vhost with servername subdomain based on Django username and activates the vhost
- Create subdomain on Hetzner using their api
- Create SSL certificate for the subdomain with certbot
- Asign random ports for Docker and insert them on .env to be used in docker-compose.yml variables
- Asign Django username to container names in .env to be used in docker-compose.yml variables
- Launh Docker Compose 
- User receives subdomain to access workspace on Django dashboard


## admin Decidim

admin@example.org
decidim123456789

## Deployment

```
git clone https://gitlab.com/abidueiro/django-workspaces-docker.git
cd django-workspaces-docker
pip install -r requirements.txt

```
## Requirements

- GNU/Linux server with enough RAM to run as much workspaces you plan to have running at same time. 
- Docker and Docker-Compose instaleed
- Recommended python virtualenv
- Hetzner DNS console or any other Domain name provider that supports interacting via api
- Edit views.py and add fill up variables domain_name = "wit_your_domain_name", hetzner_api = "your_hetzner_token", hetzner_zone_id = "zone_id_for_domain"
- To work without superuser privileges run `visudo`and add at the end of the file following lines changing "username" for user on your server

```
username ALL=(ALL) NOPASSWD:/usr/bin/nginx
username ALL=(ALL) NOPASSWD:/usr/bin/certbot
username ALL=(ALL) NOPASSWD:/usr/bin/ln
```

## To be Done

- Better templates
- Add functions to delete workspaces
- Hability for user to download files from Django dashboard
 
