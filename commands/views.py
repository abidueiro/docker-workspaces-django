# Create your views here.

from django.shortcuts import render
import subprocess
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from urllib import parse
import random
import string
import os
import errno
import requests
import json
from django.shortcuts import redirect
from django.urls import reverse

domain_name = ""
hetzner_api = ""
hetzner_zone_id = ""
server_ip = ""

def index(request):
     return redirect(reverse('admin:index'))


def mkdir(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise
        
def clone_repo(repo_url, repo_name):
    subprocess.call(['git', 'clone', repo_url, repo_name])

def container_variables_jinja2():
    # definir nombre de contenedor
    os.environ["CONTAINER_NAME"] = f"{username}_workspaces_docker"
    # definir dos puertos aleatorios contendore
    os.environ["PORT_1"] = str(random.randint(8000, 9000))
    os.environ["PORT_2"] = str(random.randint(8000, 9000))

# definir variables vhost nginx con jinja2
def vhost_nginx():
    # crear nginx vhost proxypass para {{ username }} Django.
    with open(f'/etc/nginx/sites-available/{username}.{domain_name}', 'w') as f:
        f.write(f"""server {{
    listen 80;
    server_name {username}.{domain_name}};

    location / {{
        proxy_pass http://127.0.0.1:{os.environ["PORT_1"]};
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme; 
        }}
    }}
""")

# insertar variables CONTAINER_NAME, PORT_1 y PORT_2 en docker-compose.yml
def docker_compose():
    with open(f"/home/alfons/{username}/decidim_workspaces_docker/.env", 'w') as f:
        f.write(f"""
PORT={os.environ["PORT_1"]}
PORT_1={os.environ["PORT_1"]}
PORT_2={os.environ["PORT_2"]}
CONTAINER_APP_NAME: {username}_workspaces_app_docker
CONTAINER_DB_NAME: {username}_workspaces_db_docker
""")


# Crear subdominio con la api de Hetzner
def create_subdomain(username):
    # Create Record
    # POST https://dns.hetzner.com/api/v1/records

    try:
        response = requests.post(
            url="https://dns.hetzner.com/api/v1/records",
            headers={
                "Content-Type": "application/json",
                "Auth-API-Token": hetzner_api,
            },
            data=json.dumps({
                "value": server_ip,
                "ttl": 300,
                "type": "A",
                "name": username,
                "zone_id": hetzner_zone_id
            })
        )
        print('Response HTTP Status Code: {status_code}'.format(
            status_code=response.status_code))
        print('Response HTTP Response Body: {content}'.format(
            content=response.content))
    except requests.exceptions.RequestException:
        print('HTTP Request failed')


# Crear certificado SSL con certbot nginx
def certbot_nginx():
    subprocess.call(['certbot', '--nginx', '--non-interactive', '--agree-tos', '--email', 'alfons.marques@colectic.coop', "-d", username + "." + {domain_name}])


@csrf_exempt
@login_required
def dashboard_view(request):
    if request.method == 'POST':
        user = request.user 
        message = f"Hello, {user.username}!"
        url = f"{user.username}.{domain_name}"
        global username
        username = str(user.username)
        # crear directorio de usuario
        mkdir(f"/home/alfons/{username}")
        # clonar repositorio
        clone_repo("https://gitlab.com/abidueiro/decidim-hacks.git", f"/home/alfons/{username}/decidim_workspaces_docker")
        # cambiar al directorio de trabajo
        os.chdir(f"/home/alfons/{username}/decidim_workspaces_docker")
        # show current directory
        print(os.getcwd())
        # definir variables contenedor de jinja2
        print("#######definiendo variables jinja2#######")
        container_variables_jinja2()
        docker_compose()
        # ejecutar contenedor
        print("#######ejecutando contenedor#######")
        subprocess.call(["docker-compose", "-f", f"/home/alfons/{username}/decidim_workspaces_docker/docker-compose.yml", "--project-name", f"{username}", "up", "-d"])
        # crear subdominio
        print("#######creando subdominio#######")
        create_subdomain(username)
        # crear nginx vhost
        print("#######creando nginx vhost#######")
        vhost_nginx()
        # habilitando el vhost de nginx
        print("#######habilitando el vhost de nginx#######")
        subprocess.call(["sudo", "ln", "-s", f"/etc/nginx/sites-available/{username}.{domain_name}", "/etc/nginx/sites-enabled/"])
        # reiniciar nginx
        print("#######reiniciando nginx#######")
        subprocess.call(["sudo", "nginx", "-s", "reload"])
        # crear certificado SSL
        print("#######creando certificado SSL#######")
        certbot_nginx()
        # renderizar dashboard
        return render(request, 'user_area/dashboard.html', {'message': message, 'url': url})
    else:
        user = request.user
        message = f"Bienvenido a tu area de trabajo, {user.username}!"
        return render(request, 'user_area/user_welcome.html', {'message': message})


