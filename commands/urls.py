from . import views
from .views import dashboard_view
from django.urls import path, include

urlpatterns = [
     path('user_area/', views.dashboard_view, name='dashboard'),
     path('', views.index, name='index'),
]

# (template_name='admin/login.html', auth_views.LoginView.as_view(template_name='admin/index.html')),