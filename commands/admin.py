from django.contrib import admin

# Register your models here.

from django.urls import reverse_lazy
from django.views.generic.base import RedirectView

admin.site.index_template = 'admin/index.html'